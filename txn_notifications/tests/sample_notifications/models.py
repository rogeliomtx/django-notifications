from txn_notifications.base.models import (
    AbstractCategory,
    AbstractTemplate,
    AbstractNotification,
    AbstractUserHandlerSetting,
    AbstractUserCategorySetting,
)


class Category(AbstractCategory):
    class Meta(AbstractCategory.Meta):
        abstract = False


class Template(AbstractTemplate):
    class Meta(AbstractTemplate.Meta):
        abstract = False


class Notification(AbstractNotification):
    class Meta(AbstractNotification.Meta):
        abstract = False


class UserCategorySetting(AbstractUserCategorySetting):
    class Meta(AbstractUserCategorySetting.Meta):
        abstract = False


class UserHandlerSetting(AbstractUserHandlerSetting):
    class Meta(AbstractUserHandlerSetting.Meta):
        abstract = False

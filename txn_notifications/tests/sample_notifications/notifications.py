from txn_notifications.handlers import DjangoHandler
from txn_notifications.wrappers import NotificationWrapper


class VerifyUserNotification(NotificationWrapper):
    # template
    template_name = "Verificar usuario"
    template_description = "Envía un correo o sms para verificar el usuario"
    template_slug = "verify_user"
    template_category = "users"

    # default options
    title = "Verifica tu cuenta"
    is_active = True

    # mailgun notification
    mailgun_body = """Hola **{{ user.first_name }}**,
    Verifica tu cuenta en el siguiente enlace:
    """
    mailgun_url = (
        "https://wwww.app.example.com/users/verify/{{ user.verify_code }}/"
    )
    mailgun_url_msg = "Verificar mi cuenta"

    # twilio notification
    twilio_body = """Hola **{{ user.first_name }}**,
    Verifica tu cuenta en el siguiente enlace:
    """
    twilio_url = (
        "https://wwww.app.example.com/users/verify/{{ user.verify_code }}/"
    )
    twilio_url_msg = "Verificar mi cuenta"

    supported_handlers = [DjangoHandler]


def notify_verify_user(recipient, *, sender=None, verification_code=None):
    notification = VerifyUserNotification()
    notification.send(
        recipient=recipient,
        sender=sender,
        data=dict(
            verification_code=verification_code,
        ),
    )

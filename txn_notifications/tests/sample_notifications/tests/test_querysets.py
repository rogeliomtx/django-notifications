from django.test import override_settings

from txn_notifications.tests.test_querysets import (
    NotificationQSTestCase,
    UserNotificationQSTestCase,
)


@override_settings(SAMPLE=True)
class NotificationQSTest(NotificationQSTestCase):
    pass


@override_settings(SAMPLE=True)
class UserNotificationQSTest(UserNotificationQSTestCase):
    pass

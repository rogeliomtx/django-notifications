from django.test import override_settings

from txn_notifications.tests.test_wrapper import NotificationWrapperTestCase


@override_settings(SAMPLE=True)
class NotificationWrapperTest(NotificationWrapperTestCase):
    pass

from django.test import override_settings

from txn_notifications.tests.test_models import NotificationMarkTestCase


@override_settings(SAMPLE=True)
class NotificationMarkTest(NotificationMarkTestCase):
    pass

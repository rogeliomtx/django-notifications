from django.test import override_settings

from txn_notifications.tests.test_template import TemplateTestCase


@override_settings(SAMPLE=True)
class TemplateTest(TemplateTestCase):
    pass

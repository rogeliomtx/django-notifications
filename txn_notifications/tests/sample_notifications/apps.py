from django.apps import AppConfig


class SampleNotificationsConfig(AppConfig):
    name = "txn_notifications.tests.sample_notifications"
    label = "sample_notifications"
    default_auto_field = "django.db.models.BigAutoField"

from django.http import HttpResponse
from django.test import TestCase

from txn_notifications.exceptions import NotificationNotSent
from txn_notifications.tests.factories import UserFactory, TemplateFactory
from txn_notifications.handlers.generic import Handler


class GenericHandler(Handler):
    slug = "test"
    recipient_attr = "email"


class HandlerTestCase(TestCase):
    def setUp(self):
        self.recipient = UserFactory()
        self.sender = UserFactory()
        self.template = TemplateFactory()

    def test_send(self):
        handler = GenericHandler(
            recipient=self.recipient,
            template=self.template,
            sender=self.sender,
            data={},
        )
        try:
            handler.send()
            self.fail("NotImplementedError expected")
        except NotImplementedError:
            pass

    def test_send__no_slug(self):
        try:
            handler = Handler(
                recipient=self.recipient,
                template=self.template,
                sender=self.sender,
                data={},
            )
            handler.send()
            self.fail("Exception expected")
        except NotificationNotSent:
            pass

    def test_perform_send(self):
        handler = GenericHandler(
            recipient=self.recipient,
            template=self.template,
            sender=self.sender,
            data={},
        )
        try:
            handler.perform_send()
            self.fail("NotImplementedError expected")
        except NotImplementedError:
            pass

    def test_get_prov_message_id(self):
        handler = GenericHandler(
            recipient=self.recipient,
            template=self.template,
            sender=self.sender,
            data={},
        )
        try:
            handler.get_prov_id(None)
            self.fail("NotImplementedError expected")
        except NotImplementedError:
            pass

    def test_get_prov_status(self):
        handler = GenericHandler(
            recipient=self.recipient,
            template=self.template,
            sender=self.sender,
            data={},
        )
        try:
            handler.get_prov_status(None)
            self.fail("NotImplementedError expected")
        except NotImplementedError:
            pass

    def test_post_send__no_record__no_check_status(self):
        template = TemplateFactory(
            slug="test__django", record=False, check_status=False
        )

        handler = GenericHandler(
            recipient=self.recipient,
            template=template,
            sender=self.sender,
            data={},
        )

        response = HttpResponse()
        handler.post_send(response)
        self.assertIsNone(handler.notification)

    def test_post_send__record(self):
        template = TemplateFactory(
            slug="test__django", record=True, check_status=False
        )

        handler = GenericHandler(
            recipient=self.recipient,
            template=template,
            sender=self.sender,
            data={},
        )

        response = HttpResponse()
        handler.post_send(response)
        self.assertIsNotNone(handler.notification)
        self.assertIsNotNone(handler.notification.id)

    def test_post_send__record__check_status(self):
        template = TemplateFactory(
            slug="test__django", record=True, check_status=True
        )

        handler = GenericHandler(
            recipient=self.recipient,
            template=template,
            sender=self.sender,
            data={},
        )

        try:
            response = HttpResponse()
            handler.post_send(response)
            self.fail("NotImplementedError expected")
        except NotImplementedError:
            pass

    def test_get_title(self):
        handler = GenericHandler(
            recipient=self.recipient,
            template=self.template,
            sender=self.sender,
            data={
                "sender": self.sender,
                "recipient": self.recipient,
                "template": self.template,
            },
        )

        self.assertEqual(
            handler.get_title(), f"hi {self.recipient.first_name}!"
        )

    def test_get_body(self):
        handler = GenericHandler(
            recipient=self.recipient,
            template=self.template,
            sender=self.sender,
            data={
                "context": {
                    "sender": self.sender,
                    "recipient": self.recipient,
                    "template": self.template,
                }
            },
        )
        self.assertEqual(handler.get_body(), self.template.body)

    def test_get_url(self):
        handler = GenericHandler(
            recipient=self.recipient,
            template=self.template,
            sender=self.sender,
            data={
                "sender": self.sender,
                "recipient": self.recipient,
                "template": self.template,
            },
        )
        self.assertEqual(
            handler.get_url(),
            f"https://app.test.mx/profile/{self.recipient.id}/",
        )

    def test_get_url_msg(self):
        handler = GenericHandler(
            recipient=self.recipient,
            template=self.template,
            sender=self.sender,
            data={
                "sender": self.sender,
                "recipient": self.recipient,
                "template": self.template,
            },
        )
        self.assertEqual(handler.get_url_msg(), self.template.url_msg)

    def test_get_recipient(self):
        handler = GenericHandler(
            recipient=self.recipient,
            template=self.template,
            sender=self.sender,
            data={
                "sender": self.sender,
                "recipient": self.recipient,
                "template": self.template,
            },
        )
        self.assertEqual(handler.get_recipient(), self.recipient.email)

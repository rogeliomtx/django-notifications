from django.core import mail
from django.test import TestCase, override_settings

from txn_notifications.handlers.email import EmailHandler
from txn_notifications.tests.factories import UserFactory, TemplateFactory


@override_settings(
    DEFAULT_FROM_EMAIL="Test <test@test.com>",
    EMAIL_BACKEND="django.core.mail.backends.locmem.EmailBackend",
)
class EmailHandlerTestCase(TestCase):
    def setUp(self):
        self.recipient = UserFactory(email="recipient@test.com")
        self.sender = UserFactory(email="sender@test.com")
        self.template = TemplateFactory(
            body="this is a **mark_down** template {{foo}} {{x}}",
        )

        self.data = dict(
            recipient=self.recipient,
            template=self.template,
            sender=self.sender,
            data={
                "recipient": self.recipient,
                "sender": self.sender,
                "template": self.template,
                "foo": "bar",
                "x": 100,
                "from_email": "FROM EMAIL <from@test.com>",
                "bcc": ["bbc@test.com"],
                "cc": ["cc@test.com"],
                "reply_to": ["reply_to@test.com"],
                "headers": {"a": "b"},
                # alternatives=""
                "context": {
                    # txt_template="",
                    # html_template="",
                },
            },
        )

        self.handler = EmailHandler(**self.data)

    def test_send(self):
        self.handler.send()
        self.assertEqual(len(mail.outbox), 1)

        email = mail.outbox[0]
        data = self.data.get("data")
        self.assertEqual(email.from_email, data.get("from_email"))
        self.assertEqual(email.to, [self.recipient.email])
        self.assertEqual(email.subject, self.handler.get_title())
        self.assertEqual(email.bcc, data.get("bcc"))
        self.assertEqual(email.cc, data.get("cc"))
        self.assertEqual(email.reply_to, data.get("reply_to"))
        self.assertEqual(email.extra_headers, data.get("headers"))

        self.assertEqual(
            email.body.replace("\n", " ").replace("  ", " "),
            f"hi {self.recipient.first_name}! "
            f"this is a mark_down template {self.data.get('data').get('foo')} "
            f"{self.data.get('data').get('x')} See your profile "
            f"https://app.test.mx/profile/{self.recipient.pk}/",
        )

from django.apps import AppConfig


class NotificationsConfig(AppConfig):
    name = "txn_notifications"
    default_auto_field = "django.db.models.BigAutoField"
